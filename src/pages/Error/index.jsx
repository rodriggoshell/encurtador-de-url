import './error.css';
import { Link } from 'react-router-dom';


function Error() {
  return(
    <div className="container-error">
      <img src="./public/notfound.png" alt="Imagem de Erro"/>
      <h1>Pagina nao encontrada</h1>
      <Link to="/">
        Inicio
      </Link>
    </div>
  );
}

export default Error;
