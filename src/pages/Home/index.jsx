import { useState } from 'react';
import './home.css';
import './menu.css';
import { FiLink } from 'react-icons/fi';
import { BsInstagram } from 'react-icons/bs';
import { Link } from 'react-router-dom';

import Modal from '../Modal';

function Menu() {
  return(
    <div className="menu">
      <a href="#" className="social">
        <BsInstagram color="#FFF" size={24}/>
      </a>
      <Link to="/links" className="menu-item">
        Meus Links
      </Link>
    </div>
  );
}

function Home() {
  
  const [link, setLink] = useState('');
  
  function click(e) {
    alert('Subiu!!')
  }

  return(
    <div className="container-home">
      <div className="logo">
        <img src="./public/logo.png" alt="Logo" />
        <h1>Encurtador de link</h1>
        <span>Cole seu link aqui</span>
  </div>
      <div className="area-input">
        <div>
          <FiLink size={24} color="#fff"/>
          <input type="text" 
            placeholder="cole o link aqui"
            value={link}
            onChange={ (e)=> setLink(e.target.value)}
          />
        </div>
        <button onClick={click}>Encurtar</button>
      </div>
      <Menu />
      <Modal />
    </div> 
  );
}

export default Home;
