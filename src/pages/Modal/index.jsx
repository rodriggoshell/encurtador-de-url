import './link-item.css';
import { FiX, FiClipboard } from 'react-icons/fi'

function Modal(){
  return(
    <div className="modal-container">
      <div className="modal-header">
        <h2>Link encurtado</h2>
        <button>
          <FiX size={25} color="#000" />
        </button>
      </div>
      <span>
        https://www.google.com
      </span> 
      <button className="btn-modal">
	https://google.com
        <FiClipboard size={20} color="#FFF" />
      </button> 
    </div>
  );
}

export default Modal;
